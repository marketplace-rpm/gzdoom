# Information / Информация

SPEC-файл для создания RPM-пакета **gzdoom**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/gzdoom`.
2. Установить пакет: `dnf install gzdoom`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)